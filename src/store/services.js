import Axios from 'axios'
import BookService from '../services/BookService'

const apiUrl = 'https://localhost:44325/api/'

// Axios Configuration
Axios.defaults.headers.common.Accept = 'application/json'

export default {
  bookService: new BookService(Axios, apiUrl)
}
