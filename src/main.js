import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue, BootstrapVueIcons, IconsPlugin } from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PortalVue from 'portal-vue'
import './assets/app.scss'
import './assets/scss/argon.scss'
import './assets/vendor/nucleo/css/nucleo.css'
import './components/_global'
import Header from './components/layout/header.vue'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(IconsPlugin)
Vue.use(PortalVue)

Vue.component('Header', Header)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
