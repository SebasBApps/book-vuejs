class BookService {
    axios
    baseUrl

    constructor (axios, apiUrl) {
      this.axios = axios
      this.baseUrl = `${apiUrl}Books`
    }

    getAll (criteria) {
      const self = this
      console.log('service: ' + criteria)
      return self.axios.get(`${self.baseUrl}`, {
        params: {
          filter: criteria
        }
      })
    }
}

export default BookService
